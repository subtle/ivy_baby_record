// import 'package:flutter/material.dart';
// import 'package:flutter/widgets.dart';
// import 'package:ivy_baby_record_example/test_model.dart';
// import 'package:ivy_baby_record_example/test_widget.dart';
//
// class TestInheritedWidget extends InheritedWidget {
//   TestInheritedWidget({
//     Key key,
//     @required this.data,
//     @required Widget child,
//   }): super(key: key, child: child);
//
//   final int data;
//
//   static TestInheritedWidget of(BuildContext context) {
//     // return context.dependOnInheritedWidgetOfExactType<TestInheritedWidget>();
//     return context.getElementForInheritedWidgetOfExactType<TestInheritedWidget>().widget;
//   }
//
//   @override
//   bool updateShouldNotify(TestInheritedWidget oldWidget) {
//     return oldWidget.data != data;
//   }
// }
//
// class InheritedWidgetTestRoute extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() {
//     return _InheritedWidgetTestRouteState();
//   }
// }
//
// class _InheritedWidgetTestRouteState extends State<InheritedWidgetTestRoute> {
//   int _count = 0;
//
//   @override
//   Widget build(BuildContext context) {
//     return Center(
//       child: TestInheritedWidget(
//         data: _count,
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             Padding(
//               padding: const EdgeInsets.only(bottom: 20.0),
//               child: TestWidget(),//子widget中依赖ShareDataWidget
//             ),
//             ElevatedButton(
//               child: Text("Increment"),
//               //每点击一次，将count自增，然后重新build,ShareDataWidget的data将被更新
//               onPressed: () => setState(() => ++_count),
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
//
// class InheritedProvider<T> extends InheritedWidget {
//   InheritedProvider({
//     @required this.data,
//     @required Widget child
//   }) : super(child: child);
//
//   final T data;
//
//   @override
//   bool updateShouldNotify(InheritedProvider<T> oldWidget) {
//     // return oldWidget.data != data;
//     return true;
//   }
// }
//
// class TestProvider<T extends ChangeNotifier> extends StatefulWidget {
//   TestProvider({
//     Key key,
//     this.data,
//     this.child}) {
//     print("TestProvider init : $data");
//   }
//
//   final Widget child;
//   final T data;
//
//   static T of<T>(BuildContext context, {bool listen = true}) {
//     // final type = _typeOf<InheritedProvider<T>>();
//     final provider = listen ? context.dependOnInheritedWidgetOfExactType<InheritedProvider<T>>() :
//     context.getElementForInheritedWidgetOfExactType<InheritedProvider<T>>().widget as InheritedProvider<T>;
//     return provider.data;
//   }
//
//   @override
//   _TestProviderState<T> createState() {
//     return _TestProviderState();
//   }
// }
//
// class _TestProviderState<T extends ChangeNotifier> extends State<TestProvider<T>> {
//   void update() {
//     //如果数据发生变化（model类调用了notifyListeners），重新构建InheritedProvider
//     setState(() => {
//       print("setState update provider")
//     });
//   }
//
//   @override
//   void didUpdateWidget(TestProvider<T> oldWidget) {
//     //当Provider更新时，如果新旧数据不"=="，则解绑旧数据监听，同时添加新数据监听
//     if (widget.data != oldWidget.data) {
//       oldWidget.data.removeListener(update);
//       widget.data.addListener(update);
//     }
//     super.didUpdateWidget(oldWidget);
//     print("didUpdateWidget provider");
//   }
//
//   @override
//   void initState() {
//     // 给model添加监听器
//     widget.data.addListener(update);
//     super.initState();
//     print("initState provider");
//   }
//
//   @override
//   void dispose() {
//     // 移除model的监听器
//     widget.data.removeListener(update);
//     super.dispose();
//     print("dispose provider");
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     print("build provider");
//     return InheritedProvider<T>(
//       data: widget.data,
//       child: widget.child,
//     );
//   }
// }
//
// class ProviderRoute extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() {
//     return _ProviderRouteState();
//   }
// }
//
// class _ProviderRouteState extends State<ProviderRoute> {
//   @override
//   Widget build(BuildContext context) {
//     return Center(
//       child: TestProvider<TestModel>(
//         data: TestModel(),
//         child: Builder(builder: (context) {
//           return Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Consumer<TestModel>(builder: (context, model) => Text(model.count.toString())),
//
//               Builder(builder: (context) {
//                 print("build raised button");
//                 return ElevatedButton(
//                   child: Text("Increment"),
//                   //每点击一次，将count自增，然后重新build,ShareDataWidget的data将被更新
//                   onPressed: () => {
//                     TestProvider.of<TestModel>(context, listen: false).increase()
//                   },
//                 );
//               })
//             ],
//           );
//         }),
//       )
//     );
//   }
//
// }
//
// // 这是一个便捷类，会获得当前context和指定数据类型的Provider
// class Consumer<T> extends StatelessWidget {
//   Consumer({
//     Key key,
//     @required this.builder,
//   }) : super(key: key);
//
//   final Widget Function(BuildContext context, T value) builder;
//
//   @override
//   Widget build(BuildContext context) {
//     return builder(
//       context,
//       TestProvider.of<T>(context),
//     );
//   }
// }
//
// class StreamBuilderTestRoute extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() {
//     return _StreamBuilderTestRouteState();
//   }
// }
//
// class _StreamBuilderTestRouteState extends State<StreamBuilderTestRoute> {
//   @override
//   Widget build(BuildContext context) {
//     return StreamBuilder<int>(
//       stream: counter(), //
//       //initialData: ,// a Stream<int> or null
//       builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
//         if (snapshot.hasError)
//           return Text('Error: ${snapshot.error}');
//         switch (snapshot.connectionState) {
//           case ConnectionState.none:
//             return Text('没有Stream');
//           case ConnectionState.waiting:
//             return Text('等待数据...');
//           case ConnectionState.active:
//             return Text('active: ${snapshot.data}');
//           case ConnectionState.done:
//             return Text('Stream 已关闭');
//         }
//         return null; // unreachable
//       },
//     );
//   }
//
//   Stream<int> counter() {
//     return Stream.periodic(Duration(seconds: 1), (i) {
//       return i;
//     });
//   }
// }