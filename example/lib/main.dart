import 'package:flutter/material.dart';
import 'dart:async';

import 'package:ivy_baby_record/user/pages/init/init_child.dart';
import 'package:ivy_baby_record/user/pages/login/reg_login.dart';
import 'package:ivy_baby_record/user/pages/record/record_home.dart';
import 'package:ivy_baby_record/user/pages/splash/splash.dart';
import 'package:ivy_baby_record_example/calculator_test.dart';
import 'package:ivybaby_api/api/config/global_data.dart';
import 'package:ivybaby_api/api/config/api_config.dart';
import 'package:ivy_baby_record/user/db/database_util.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  ApiConfig.inst.init();

  await DatabaseUtil.inst.init();

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, WidgetBuilder> _routes = {
    '/' : (context) => SplashPage(),
    // '/' : (context) => StreamBuilderTestRoute(),
    'init' : (context) => InitChildPage(),
    'login' : (context) => RegLoginPage({'isReg' : false}),
    'record' : (context) => RecordHomePage(),
  };

  @override
  void initState() {
    super.initState();

    GlobalData.appDevice.deviceNo = 'B4:F1:DA:B3:48:39';
    GlobalData.appDevice.uuid = '19d430e3-08dd-4a38-9dc9-e829f87bb28f';
    GlobalData.appDevice.loginType = '5';
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: _routes,
      initialRoute: '/',
    );
  }
}
