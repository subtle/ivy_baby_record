import 'package:flutter/cupertino.dart';

class TestModel extends ChangeNotifier {
  int count = 0;

  void increase() {
    count = count + 1;
    notifyListeners();
  }
}