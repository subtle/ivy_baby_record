// import 'package:flutter/widgets.dart';
// import 'package:ivy_baby_record_example/calculator_test.dart';
//
// class TestWidget extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() {
//     return _TestWidgetState();
//   }
// }
//
// class _TestWidgetState extends State<TestWidget> {
//   @override
//   Widget build(BuildContext context) {
//     print("build _TestWidgetState");
//     return Text(TestInheritedWidget.of(context).data.toString());
//   }
//
//   @override
//   void initState() {
//     super.initState();
//     print("initState");
//   }
//
//   @override
//   void didChangeDependencies() {
//     super.didChangeDependencies();
//     print("didChangeDependencies");
//   }
//
//   @override
//   void didUpdateWidget(covariant TestWidget oldWidget) {
//     super.didUpdateWidget(oldWidget);
//     print("didUpdateWidget");
//   }
//
//   @override
//   void deactivate() {
//     super.deactivate();
//     print("deactivate");
//   }
//
//   @override
//   void reassemble() {
//     super.reassemble();
//     print("reassemble");
//   }
//
//   @override
//   void dispose() {
//     super.dispose();
//     print("dispose");
//   }
// }