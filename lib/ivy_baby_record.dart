
import 'dart:async';

import 'package:flutter/services.dart';

class IvyBabyRecord {
  static const MethodChannel _channel =
      const MethodChannel('ivy_baby_record');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String> get clientVersion async {
    final String version = await _channel.invokeMethod('getClientVersion');
    return version;
  }

  static Future<int> loadImage(String name) async {
    final int textureId = await _channel.invokeMethod('loadImage', {
      'url' : name
    });
    return textureId;
  }

  static Future<bool> release(String url) async {
    final bool result = await _channel.invokeMethod('release', {
      'url' : url
    });
    return result;
  }

  static Future<bool> releaseList(List<String> url) async {
    final bool result = await _channel.invokeMethod('releaseList', {
      'url' : url
    });
    return result;
  }
}
