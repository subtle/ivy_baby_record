import 'package:ivy_baby_record/user/model/account.dart';
import 'package:ivy_baby_record/user/model/child.dart';
import 'package:ivy_baby_record/user/model/member.dart';
import 'package:ivybaby_api/api/base/base_ivybaby_response.dart';
import 'package:json_annotation/json_annotation.dart';
part 'register_fast.g.dart';

@JsonSerializable()
class RegisterRsp extends IvyBabyResponse<RegisterRspData> {
  RegisterRsp() : super();

  factory RegisterRsp.fromJson(Map<String, dynamic> json) => _$RegisterRspFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterRspToJson(this);
}

@JsonSerializable()
class RegisterRspData {
  RegisterRspData();

  String? userToken;

  List<Child>? memberChilds;

  List<Child>? accountChilds;

  Member? member;

  Account? account;

  int? isRegister = 0;

  factory RegisterRspData.fromJson(Map<String, dynamic> json) => _$RegisterRspDataFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterRspDataToJson(this);
}
