import 'package:ivy_baby_record/user/model/record/day_log.dart';
import 'package:ivybaby_api/api/base/base_ivybaby_response.dart';
import 'package:json_annotation/json_annotation.dart';
part 'day_log_rsp.g.dart';

@JsonSerializable()
class GetDayLogRsp extends IvyBabyResponse<GetDayLogRspData> {
  GetDayLogRsp() : super();

  factory GetDayLogRsp.fromJson(Map<String, dynamic> json) => _$GetDayLogRspFromJson(json);

  Map<String, dynamic> toJson() => _$GetDayLogRspToJson(this);
}

@JsonSerializable()
class GetDayLogRspData {
  late List<DayLog> daylog;

  late List<DeleteDayLog> dellist;

  GetDayLogRspData();

  factory GetDayLogRspData.fromJson(Map<String, dynamic> json) => _$GetDayLogRspDataFromJson(json);

  Map<String, dynamic> toJson() => _$GetDayLogRspDataToJson(this);
}

@JsonSerializable()
class DeleteDayLog {
  late String id;

  int type = 0;

  DeleteDayLog();

  factory DeleteDayLog.fromJson(Map<String, dynamic> json) => _$DeleteDayLogFromJson(json);

  Map<String, dynamic> toJson() => _$DeleteDayLogToJson(this);
}