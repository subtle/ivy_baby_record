import 'package:ivy_baby_record/user/model/record/has_record.dart';
import 'package:ivybaby_api/annotation/api_generator_annotation.dart';

import 'day_log_rsp.dart';

/// import 'dart:convert';
/// import 'package:ivybaby_api/api/config/api_config.dart';
/// import 'package:ivybaby_api/api/base/api_base.dart';
/// import 'package:ivybaby_api/api/config/global_data.dart';
@Api('\${ApiConfig.ivyBase}', target: 'RecordApiImpls')
abstract class RecordApiService {
  static const String BABY_LOG = '/yxy-api-gateway/api/json/babyLog';

  @Api('$BABY_LOG/getdaylog', data: {
    'uid': '@C_userID',
    'memberId2': '@C_userID',
  })
  Future<GetDayLogRsp> getDayLog(String daytime, String userID, String childId);

  @Api('/yxy-api-gateway/api/json/gestationLog/getGravidaDaylog', data: {
    'uid': '@C_userID',
    'memberId2': '@C_userID',
  })
  Future<GetDayLogRsp> getGravidDayLog(String daytime, String userID, String childId);

  /// import 'package:ivy_baby_record/user/global/global_info.dart';
  @Api('$BABY_LOG/getHasRecords', data: {
    'uid': '@C_GlobalData.appDevice.userId.toString()',
    'memberId2': '@C_GlobalData.appDevice.userId.toString()',
    'childId': '@C_GlobalInfo.inst.childId'
  })
  Future<GetHasRecordsResponse> getHasRecords({
    int dgts = 0, int dots = 0, int dpts = 0, int dptsg = 0,
    int hgts = 0, int hots = 0, int hpts = 0, int hptsg = 0});
}