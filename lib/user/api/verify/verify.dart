import 'package:ivy_baby_record/user/api/login/register_fast.dart';
import 'package:ivy_baby_record/user/api/verify/verify_sms_code.dart';
import 'package:ivy_baby_record/user/api/yxyuser/add_account_child.dart';
import 'package:ivybaby_api/annotation/api_generator_annotation.dart';

/// import 'dart:convert';
/// import 'package:ivybaby_api/api/config/api_config.dart';
/// import 'package:ivybaby_api/api/base/api_base.dart';
/// import 'package:ivybaby_api/api/config/global_data.dart';
@Api('\${ApiConfig.ivyBase}', target: 'LoginApiImpl')
abstract class Verify {
  @Api('/yxy-api-gateway/api/json/userCenter/specialUnionLogin', data: {
    'type': 5,
    'mainPartnerId': '@C_GlobalData.appDevice.uuid',
    'secondPartnerId': '@C_GlobalData.appDevice.deviceNo',
    'partnerSex': 0
  })
  Future<RegisterRsp> specialUnionLogin();

  @Api('/yxy-api-gateway/api/json/yxyUser/addAccountChild')
  Future<AddAccountChildRsp> addAccountChild({String? birthday,
      String? deliveryType,
      String? expectedDate,
      String? gestationStatus,
      String? prematureDelivery,
      String? sex});

  @Api('/yxy-api-gateway/api/json/verifyCode/sendSMSVeriyCode', data: {
    'dialCode': '+86',
  })
  Future<VerifySMSCodeRsp> sendSMSVerifyCode(String mobile, {String type = 'register'});

  @Api('/yxy-api-gateway/api/json/userCenter/fastRegisterByMobile')
  Future<RegisterRsp> fastRegisterByMobile(String mobile, String smsVerifyCode, {String dialCode = '+86'});

  @Api('/yxy-api-gateway/api/json/userCenter/loginByMobile', )
  Future<RegisterRsp> loginByMobile(String mobile, String smsVerifyCode, {String dialCode = '+86'});
}