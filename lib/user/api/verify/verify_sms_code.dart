import 'package:ivybaby_api/api/base/base_ivybaby_response.dart';
import 'package:json_annotation/json_annotation.dart';
part 'verify_sms_code.g.dart';

@JsonSerializable()
class VerifySMSCodeRsp extends IvyBabyResponse<VerifySMSCodeData> {
  VerifySMSCodeRsp() : super();

  factory VerifySMSCodeRsp.fromJson(Map<String, dynamic> json) => _$VerifySMSCodeRspFromJson(json);

  Map<String, dynamic> toJson() => _$VerifySMSCodeRspToJson(this);
}

@JsonSerializable()
class VerifySMSCodeData {
  VerifySMSCodeData() : super();

  late int leftLimitSecond;

  factory VerifySMSCodeData.fromJson(Map<String, dynamic> json) => _$VerifySMSCodeDataFromJson(json);

  Map<String, dynamic> toJson() => _$VerifySMSCodeDataToJson(this);
}
