import 'package:ivy_baby_record/user/model/child.dart';
import 'package:ivybaby_api/api/base/base_ivybaby_response.dart';
import 'package:json_annotation/json_annotation.dart';
part 'add_account_child.g.dart';

@JsonSerializable()
class AddAccountChildRsp extends IvyBabyResponse<AddAccountChildRspData> {
  AddAccountChildRsp() : super();

  factory AddAccountChildRsp.fromJson(Map<String, dynamic> json) => _$AddAccountChildRspFromJson(json);

  Map<String, dynamic> toJson() => _$AddAccountChildRspToJson(this);
}

@JsonSerializable()
class AddAccountChildRspData {
  AddAccountChildRspData() : super();

  late Child accountChild;

  factory AddAccountChildRspData.fromJson(Map<String, dynamic> json) => _$AddAccountChildRspDataFromJson(json);

  Map<String, dynamic> toJson() => _$AddAccountChildRspDataToJson(this);
}
