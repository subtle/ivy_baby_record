import 'dart:ui';

class CustomColors {
  CustomColors._();

  static const int white = 0xFFFFFF;
  static const Color c8 = Color.fromARGB(0xFF, 0x55, 0xCE, 0xAC);
  static const Color c1 = Color.fromARGB(0xFF, 0xF5, 0xF7, 0xF5);
  static const Color c4 = Color.fromARGB(0xFF, 0x64, 0x64, 0x64);

  static const Color hint_record = Color.fromARGB(0xFF, 0xB5, 0xB5, 0xB5);
  static const Color hint_medicine = Color.fromARGB(0xFF, 0xE7, 0x78, 0x78);
  static const Color hint_medicine1 = Color.fromARGB(0xFF, 0x96, 0xDA, 0x60);
  static const Color hint_growth = Color.fromARGB(0xFF, 0x2A, 0xCE, 0x6C);

  static const Color record_feeling = Color.fromARGB(0xFF, 0x8D, 0x93, 0xEE);
  static const Color record_pregnant_medicine = Color.fromARGB(0xFF, 0x96, 0xDA, 0x60);
  static const Color record_pregnant_weight = Color.fromARGB(0xFF, 0x5A, 0xD7, 0xD7);

  static const Color record_vertical_divider = Color.fromARGB(0xFF, 0x81, 0xEA, 0xEA);
}