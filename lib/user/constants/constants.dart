class Constants {
  static const int TYPE_GUEST = 5;
  static const String GUEST_MEM_ID = '0';

  static const int TYPE_1SECOND = 0;
  static const int TYPE_BREASTFEED_FORMULA = -1;
  static const int TYPE_UNDEFINED = 0;
  static const int TYPE_BREASTFEED = 1;
  static const int TYPE_BOTTLE_FORMULA = 2;
  static const int TYPE_BOTTLE_BREASTFEED = 3;
  static const int TYPE_ADDITIONAL = 4;
  static const int TYPE_DEFECATE = 5;
  static const int TYPE_SLEEP = 6;
  static const int TYPE_GROW = 7;
  static const int TYPE_PHOTO = 8;
  static const int TYPE_WAKE = 9;

  static const int TYPE_PREGNANCY_WEIGHT = 50;
  static const int TYPE_PREGNANCY_MEDICINE = 51;
  static const int TYPE_PREGNANCY_BEAT = 52;
  static const int TYPE_PREGNANCY_FEELING = 53;

  static const int TYPE_HAS_RECORD_GROW = 1;
  static const int TYPE_HAS_RECORD_OTHER = 2;
  static const int TYPE_HAS_RECORD_MEDICINE = 3;
  static const int TYPE_HAS_RECORD_MEDICINE_PREGNANT = 4;
  static const int TYPE_HAS_RECORD_OTHER_PREGNANT = 5;
}