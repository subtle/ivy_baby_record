import 'dart:async';

import 'package:floor/floor.dart';
import 'package:ivy_baby_record/user/model/app_settings.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'dao/dao_setting.dart';
part 'app_database.g.dart';

@Database(
    version: 1,
    entities: [
      Profile
    ])
abstract class AppDatabase extends FloorDatabase {
  ProfileDao get settingDao;
}