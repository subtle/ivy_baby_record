import 'dart:convert';

import 'package:floor/floor.dart';
import 'package:ivy_baby_record/user/model/record/day_log.dart';

class DataInfoConverter extends TypeConverter<DataInfo, String> {
  @override
  DataInfo decode(String databaseValue) {
    return DataInfo.fromJson(json.decode(databaseValue));
  }

  @override
  String encode(DataInfo value) {
    return json.encode(value);
  }
}