import 'dart:convert';

import 'package:floor/floor.dart';
import 'package:ivy_baby_record/user/model/record/day_log.dart';

class DayLogImageConverter extends TypeConverter<List<DayLogImage>, String> {
  @override
  List<DayLogImage> decode(String databaseValue) {
    return (json.decode(databaseValue) as List).map((e) => DayLogImage.fromJson(e as Map<String, dynamic>)).toList();
  }

  @override
  String encode(List<DayLogImage> value) {
    return json.encode(value);
  }
}