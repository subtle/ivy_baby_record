import 'package:floor/floor.dart';
import 'package:ivy_baby_record/user/model/account.dart';

@dao
abstract class AccountDao {
  @Query('SELECT * FROM account')
  Future<List<Account>> findAllAccounts();

  @Query('SELECT * FROM account WHERE id = :id')
  Future<Account?> findAccountById(String id);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<int> insertAccount(Account account);

  @delete
  Future<int> deleteAccount(Account account);

  @update
  Future<int> updateAccount(Account account);
}