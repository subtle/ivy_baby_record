import 'package:floor/floor.dart';
import 'package:ivy_baby_record/user/model/child.dart';

@dao
abstract class ChildDao {
  @Query('SELECT * FROM child')
  Future<List<Child>> findAllChildren();

  @Query('SELECT * FROM child WHERE id = :id')
  Future<Child?> findChildById(String id);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<int> insertChild(Child child);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<List<int>> insertChildren(List<Child> child);

  @delete
  Future<int> deleteChild(Child child);

  @update
  Future<int> updateChild(Child child);
}