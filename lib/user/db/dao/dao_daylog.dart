import 'package:floor/floor.dart';
import 'package:ivy_baby_record/user/model/record/day_log.dart';

@dao
abstract class DayLogDao {
  @Query('SELECT * FROM day_log')
  Future<List<DayLog>> findAllDayLog();

  @Query('SELECT * FROM day_log WHERE date = :date')
  Future<List<DayLog>> findDayLogByDate(String date);

  @Query('SELECT * FROM day_log WHERE date >= :start AND date <= :end')
  Future<List<DayLog>> findDayLogByDateSection(String start, String end);

  @Query('SELECT * FROM day_log WHERE id = :id')
  Future<DayLog?> findDayLogById(String id);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<int> insertDayLog(DayLog dayLog);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<List<int>> insertDayLogs(List<DayLog> list);

  @delete
  Future<int> deleteDayLog(DayLog dayLog);

  @update
  Future<int> updateDayLog(DayLog dayLog);
}