import 'package:floor/floor.dart';
import 'package:ivy_baby_record/user/model/record/has_record.dart';

@dao
abstract class HasRecordDao {
  @Query('SELECT * FROM day_has_record')
  Future<List<HasRecord>> findAllHasRecord();

  @Query('SELECT * FROM day_has_record WHERE babyId = :babyId')
  Future<List<HasRecord>> findHasRecordById(String babyId);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<int> insertHasRecord(HasRecord hasRecord);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<List<int>> insertHasRecords(List<HasRecord> list);

  @delete
  Future<int> deleteHasRecord(HasRecord hasRecord);

  @update
  Future<int> updateHasRecord(HasRecord hasRecord);
}