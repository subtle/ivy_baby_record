import 'package:floor/floor.dart';
import 'package:ivy_baby_record/user/model/record/has_record.dart';

@dao
abstract class HasRecordsTsDao {
  @Query('SELECT * FROM has_record_ts WHERE babyId = :babyId')
  Future<HasRecordsTs?> findHasRecordTsByBabyId(String babyId);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<int> insertHasRecordTs(HasRecordsTs hasRecordTs);

  @delete
  Future<int> deleteHasRecordTs(HasRecordsTs hasRecordTs);

  @update
  Future<int> updateHasRecordTs(HasRecordsTs hasRecordTs);
}