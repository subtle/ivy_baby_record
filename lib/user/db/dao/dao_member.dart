import 'package:floor/floor.dart';
import 'package:ivy_baby_record/user/model/member.dart';

@dao
abstract class MemberDao {
  @Query('SELECT * FROM member')
  Future<List<Member>> findAllMembers();

  @Query('SELECT * FROM member WHERE id = :id')
  Future<Member?> findMemberById(String id);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<int> insertMember(Member member);

  @delete
  Future<int> deleteMember(Member member);

  @update
  Future<int> updateMember(Member member);
}