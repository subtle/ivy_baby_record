import 'package:floor/floor.dart';
import 'package:ivy_baby_record/user/model/app_settings.dart';

@dao
abstract class ProfileDao {
  @Query('SELECT * FROM profile')
  Future<Profile?> findProfile();

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<int> insertProfile(Profile profile);

  @delete
  Future<int> deleteProfile(Profile profile);

  @update
  Future<int> updateProfile(Profile profile);
}