import 'package:floor/floor.dart';
import 'package:ivy_baby_record/user/db/app_database.dart';

class DatabaseUtil {
  late AppDatabase _appDatabase;

  DatabaseUtil._();

  static final DatabaseUtil _instance = DatabaseUtil._();

  static DatabaseUtil get inst => _instance;

  Future<void> init() async {
    print('database init start');
    _appDatabase = await $FloorAppDatabase
        .databaseBuilder('flutter_database.db')
        .addCallback(Callback(
            onOpen: (database) {
              print('database opened');
            },
            onUpgrade: (database, start, end) {
              print('database upgrade finish');
            },
            onCreate: (database, version) {
              print('database create finish : $version');
            }))
        .build();
    print('database init end');
  }

  AppDatabase get database => _appDatabase;
}