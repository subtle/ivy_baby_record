import 'dart:async';

import 'package:floor/floor.dart';
import 'package:ivy_baby_record/user/db/dao/dao_has_record.dart';
import 'package:ivy_baby_record/user/model/account.dart';
import 'package:ivy_baby_record/user/model/child.dart';
import 'package:ivy_baby_record/user/model/member.dart';
import 'package:ivy_baby_record/user/model/record/day_log.dart';
import 'package:ivy_baby_record/user/model/record/has_record.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'dao/dao_account.dart';
import 'dao/dao_child.dart';
import 'dao/dao_daylog.dart';
import 'dao/dao_has_record_ts.dart';
import 'dao/dao_member.dart';
import 'converter/datainfo_converter.dart';
import 'converter/daylog_image_converter.dart';
part 'user_database.g.dart';

@Database(
    version: 1,
    entities: [
      Member,
      Account,
      Child,
      DayLog,
      HasRecord,
      HasRecordsTs,
    ])
abstract class UserDatabase extends FloorDatabase {
  MemberDao get memberDao;
  AccountDao get accountDao;
  ChildDao get childDao;
  DayLogDao get dayLogDao;
  HasRecordDao get hasRecordDao;
  HasRecordsTsDao get hasRecordsTsDao;
}