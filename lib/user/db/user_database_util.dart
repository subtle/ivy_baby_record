import 'package:floor/floor.dart';
import 'package:ivy_baby_record/user/db/user_database.dart';

class UserDatabaseUtil {
  late UserDatabase _userDatabase;

  UserDatabaseUtil._();

  static final UserDatabaseUtil _instance = UserDatabaseUtil._();

  static UserDatabaseUtil get inst => _instance;

  Future<void> init(String userId) async {
    print('database init start');
    _userDatabase = await $FloorUserDatabase
        .databaseBuilder(userId + '.db')
        .addCallback(Callback(
        onOpen: (database) {
          print('database opened');
        },
        onUpgrade: (database, start, end) {
          print('database upgrade finish');
        },
        onCreate: (database, version) {
          print('database create finish : $version');
        }))
        .build();
    print('database init end');
  }

  UserDatabase get database => _userDatabase;
}