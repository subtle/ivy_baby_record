import 'package:ivy_baby_record/user/model/child.dart';

class GlobalInfo {
  GlobalInfo._();

  static final GlobalInfo _instance = GlobalInfo._();

  static GlobalInfo get inst => _instance;

  Child? child;

  String get childId => child?.id??'';
}