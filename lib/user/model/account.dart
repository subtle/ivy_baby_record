import 'package:floor/floor.dart';
import 'package:json_annotation/json_annotation.dart';
part 'account.g.dart';

@Entity(tableName: 'account')
@JsonSerializable()
class Account {
  @PrimaryKey()
  String id;

  String registerIp;

  String partnerHeadImgUrl;

  String dialCode;

  String channel;

  String mobile;

  String mainPartnerId;

  String partnerNickName;

  String secondPartnerId;

  int type;

  int createAt;

  String loginName;

  String utoken;

  String lastDeviceNo;

  String memberId;

  int partnerSex;

  int status;

  Account(
      this.id,
      this.registerIp,
      this.partnerHeadImgUrl,
      this.dialCode,
      this.channel,
      this.mobile,
      this.mainPartnerId,
      this.partnerNickName,
      this.secondPartnerId,
      this.type,
      this.createAt,
      this.loginName,
      this.utoken,
      this.lastDeviceNo,
      this.memberId,
      this.partnerSex,
      this.status);

  factory Account.fromJson(Map<String, dynamic> json) => _$AccountFromJson(json);

  Map<String, dynamic> toJson() => _$AccountToJson(this);
}