import 'package:floor/floor.dart';
import 'package:json_annotation/json_annotation.dart';
part 'app_settings.g.dart';

@Entity(tableName: 'profile', withoutRowid: true)
@JsonSerializable()
class Profile {
  String lastLoginInfo;

  @PrimaryKey()
  int lastVersionCode;

  Profile(this.lastLoginInfo, this.lastVersionCode);

  factory Profile.fromJson(Map<String, dynamic> json) => _$ProfileFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileToJson(this);
}