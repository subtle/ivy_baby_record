import 'package:floor/floor.dart';
import 'package:json_annotation/json_annotation.dart';
part 'child.g.dart';

@Entity(tableName: 'child')
@JsonSerializable()
class Child {
  @PrimaryKey()
  String id;

  int birthday;

  String gestationWeek2;

  int sex;

  int deliveryType;

  String gestationWeek1;

  int expectedDate;

  int prematureDelivery;

  int createAt;

  int gestationStatus;

  int defaultStatus;

  String babyName;

  String babyIco;

  int editBirthTimes;

  String fansno;

  String memberId2;

  String babyBackIco;

  String memberId;

  Child(
      this.id,
      this.birthday,
      this.gestationWeek2,
      this.sex,
      this.deliveryType,
      this.gestationWeek1,
      this.expectedDate,
      this.prematureDelivery,
      this.createAt,
      this.gestationStatus,
      this.defaultStatus,
      this.babyName,
      this.babyIco,
      this.editBirthTimes,
      this.fansno,
      this.memberId2,
      this.babyBackIco,
      this.memberId);

  factory Child.fromJson(Map<String, dynamic> json) => _$ChildFromJson(json);

  Map<String, dynamic> toJson() => _$ChildToJson(this);
}