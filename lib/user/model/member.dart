import 'package:json_annotation/json_annotation.dart';
import 'package:floor/floor.dart';

part 'member.g.dart';

@Entity(tableName: 'member')
@JsonSerializable()
class Member {
  @PrimaryKey()
  String id;
  int birthday;
  String country;
  String backIco;
  String city;
  int level;
  String signature;
  String dialCode;
  String nickName;
  int sex;
  String mobile;
  int createAt;
  String realName;
  String qnIco;
  String ico;
  String province;
  int grade;
  String imToken;
  String tag;

  Member(
      this.id,
      this.birthday,
      this.country,
      this.backIco,
      this.city,
      this.level,
      this.signature,
      this.dialCode,
      this.nickName,
      this.sex,
      this.mobile,
      this.createAt,
      this.realName,
      this.qnIco,
      this.ico,
      this.province,
      this.grade,
      this.imToken,
      this.tag);

  factory Member.fromJson(Map<String, dynamic> json) => _$MemberFromJson(json);

  Map<String, dynamic> toJson() => _$MemberToJson(this);
}