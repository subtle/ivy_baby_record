import 'package:floor/floor.dart';
import 'package:ivy_baby_record/user/db/converter/datainfo_converter.dart';
import 'package:ivy_baby_record/user/db/converter/daylog_image_converter.dart';
import 'package:json_annotation/json_annotation.dart';
part 'day_log.g.dart';

@Entity(tableName: 'day_log', primaryKeys: ['id', 'type'])
@TypeConverters([DayLogImageConverter, DataInfoConverter])
@JsonSerializable()
class DayLog {
  DayLog(
      this.id,
      this.type,
      this.date,
      this.status,
      this.localId,
      this.startTime,
      this.eventTime,
      this.eventEndTime,
      this.note,
      this.babyId,
      this.isBackground,
      this.imgs,
      this.datainfo);

  int id;

  int type;

  String date;

  int status;

  int localId;

  String startTime;

  String eventTime;

  String eventEndTime;

  String note;

  String babyId;

  int isBackground;

  List<DayLogImage> imgs;

  DataInfo datainfo;

  factory DayLog.fromJson(Map<String, dynamic> json) => _$DayLogFromJson(json);

  Map<String, dynamic> toJson() => _$DayLogToJson(this);
}

@JsonSerializable()
class DayLogImage {
  DayLogImage();

  late int img_Id;

  late int type;

  String? url;

  String? image_key;

  late bool isDelete;

  String? createTime;

  factory DayLogImage.fromJson(Map<String, dynamic> json) => _$DayLogImageFromJson(json);

  Map<String, dynamic> toJson() => _$DayLogImageToJson(this);
}

@JsonSerializable()
class DataInfo {
  DataInfo();

  @JsonKey(defaultValue: 0)
  late int bmSid;

  @JsonKey(defaultValue: 0)
  late int bmCid;

  String? bmColor;

  @JsonKey(defaultValue: 0)
  late int bmId;

  String? bmState;

  @JsonKey(defaultValue: 0)
  late int istimer;

  @JsonKey(defaultValue: 0)
  late double gwHead;

  @JsonKey(defaultValue: 0)
  late double gwHeight;

  @JsonKey(defaultValue: 0)
  late double gwWeight;

  @JsonKey(defaultValue: 0)
  late int ml;

  String? pills;

  String? dosage;

  late int type;

  String? note;

  @JsonKey(defaultValue: 0)
  late double girth;

  @JsonKey(defaultValue: 0)
  late double weight;

  factory DataInfo.fromJson(Map<String, dynamic> json) => _$DataInfoFromJson(json);

  Map<String, dynamic> toJson() => _$DataInfoToJson(this);
}