import 'package:floor/floor.dart';
import 'package:ivybaby_api/api/base/base_ivybaby_response.dart';
import 'package:json_annotation/json_annotation.dart';
part 'has_record.g.dart';

@JsonSerializable()
class GetHasRecordsResponse extends IvyBabyResponse<HasRecordsTs> {
  GetHasRecordsResponse() : super();

  factory GetHasRecordsResponse.fromJson(Map<String, dynamic> json) => _$GetHasRecordsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetHasRecordsResponseToJson(this);
}

@Entity(tableName: 'day_has_record', primaryKeys: ['key'])
@JsonSerializable()
class HasRecord {
  String day;

  int has;

  int type;

  String key;

  String babyId;

  HasRecord(this.day, this.has, this.type, this.key, this.babyId);

  factory HasRecord.fromJson(Map<String, dynamic> json) => _$HasRecordFromJson(json);

  Map<String, dynamic> toJson() => _$HasRecordToJson(this);
}

@Entity(tableName: 'has_record_ts', primaryKeys: ['babyId'])
@JsonSerializable()
class HasRecordsTs {
  late int dgts; /// 删除的生长发育
  late int dots; /// 删除的其他
  late int dpts; /// 删除的用药
  late int dptsg;/// 删除的孕期用药

  late int hgts;
  late int hots;
  late int hpts;
  late int hptsg;

  late String babyId;

  @ignore
  late List<HasRecord> rs;

  HasRecordsTs(this.dgts, this.dots, this.dpts, this.dptsg, this.hgts,
      this.hots, this.hpts, this.hptsg, this.babyId);

  factory HasRecordsTs.fromJson(Map<String, dynamic> json) => _$HasRecordsTsFromJson(json);

  Map<String, dynamic> toJson() => _$HasRecordsTsToJson(this);
}