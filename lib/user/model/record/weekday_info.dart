class WeekdayInfo {
  DateTime start;
  DateTime end;

  WeekdayInfo(this.start, this.end);
}