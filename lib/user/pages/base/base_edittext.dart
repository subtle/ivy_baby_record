import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class EditText extends StatefulWidget {
  final String hint;
  final String content;
  final Function callback;
  final TextInputType inputType;
  final double fontSize;
  final TextAlign textAlign;
  final int maxLength;

  EditText(this.hint, this.content,
      this.callback, this.inputType, this.fontSize,
      {this.textAlign = TextAlign.start, this.maxLength = 11});

  @override
  State<StatefulWidget> createState() {
    return _EditTextState();
  }
}

class _EditTextState extends State<EditText> {
  late TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController(text: widget.content);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: TextField(
                keyboardType: widget.inputType,
                decoration: InputDecoration(
                  hintText: widget.hint,
                  hintStyle: TextStyle(fontSize: widget.fontSize, color: Colors.white54),
                  border: InputBorder.none,
                ),
                controller: _controller,
                autofocus: false,
                style: TextStyle(fontSize: widget.fontSize, color: Colors.white),
                onChanged: (value) {
                  widget.callback.call(value);
                },
                maxLines: 1,
                inputFormatters: [LengthLimitingTextInputFormatter(widget.maxLength)],
                textAlign: widget.textAlign,
              ),
            ),
          ],
        ),
        Divider(height: 1, color: Colors.white)
      ],
    );
  }
}