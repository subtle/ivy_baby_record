import 'package:extended_nested_scroll_view/extended_nested_scroll_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

abstract class BasePage extends StatefulWidget {
  BasePage({params});
}

abstract class BasePageState<T extends BasePage> extends State<T> {
  List<SingleChildWidget> _providers = [];
  TitleChangeNotifier? _titleChangeNotifier;

  @override
  void initState() {
    super.initState();
    _titleChangeNotifier = new TitleChangeNotifier(title);
    _providers.add(ChangeNotifierProvider(create: (_) => _titleChangeNotifier));
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: _providers,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: getScaffoldBody(context),
      ));
  }

  Key getContentViewKey() {
    return Key("BasePage-Content");
  }

  Widget getScaffoldBody(BuildContext context) {
    return ExtendedNestedScrollView(
      physics: NeverScrollableScrollPhysics(),
      headerSliverBuilder: (context, innerBoxIsScrolled) {
        List<Widget> list = [];
        list.add(getTitleBar(context));
        return list;
      },
      body: PrimaryScrollController(
        key: getContentViewKey(),
        controller: ScrollController(),
        child: WillPopScope(
          onWillPop: () async {
            return onKeyCodeBackClick(context);
          },
          child: buildContent(context),
        ),
      ),
    );
  }

  Widget getTitleBar(BuildContext context) {
    return SliverOffstage(
      offstage: !showTitle,
      sliver: SliverAppBar(
        backgroundColor: Colors.white,
        leading: getLeftView(context),
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: getTitleView(context),
        actions: getRightButton(context),
        floating: isTitleFloating,
        pinned: isTitlePinned,
        flexibleSpace: null,
        bottom: getUnderLineView(context) as PreferredSizeWidget?,
      ),
    );
  }

  Widget getLeftView(BuildContext context) {
    return Image(
      image: AssetImage('assets/images/title_bar_back.png',
          package: 'ivy_baby_record'),
      color: Colors.black,
    );
  }
  
  Widget? getTitleView(BuildContext context) {
    return showTitle ? Text(
        title,
        style: TextStyle(color: Colors.black38, fontSize: 16)) : null;
  }
  
  Widget? getUnderLineView(BuildContext context) {
    return isWithUnderline ? PreferredSize(
        child: Divider(
          height: 1,
          color: Color.fromARGB(0xFF, 0xE5, 0xE5, 0xE5)
        ),
        preferredSize: Size.fromHeight(1)) : null;
  }
  
  List<Widget>? getRightButton(BuildContext context) {
    return null;
  }

  void onLeftPressed(BuildContext context) {
    Navigator.of(context).pop();
  }

  String get title => '';

  bool get showTitle => true;

  bool get isTitleFloating => false;

  bool get isTitlePinned => false;

  bool get isWithUnderline => false;

  bool onKeyCodeBackClick(BuildContext context) => true;

  Widget buildContent(BuildContext context);
}

class TitleChangeNotifier extends ChangeNotifier {
  late String _title;
  TitleChangeNotifier(String content) {
    this._title = content;
  }
}