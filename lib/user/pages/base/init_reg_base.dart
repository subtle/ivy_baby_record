import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:ivy_baby_record/ivy_baby_record.dart';
import 'package:ivy_baby_record/user/util/ui_util.dart';

import 'base_page.dart';

abstract class InitRegLoginPage extends BasePage {
  InitRegLoginPage({params}) : super(params : params);
}

abstract class InitRegLoginPageState<T extends InitRegLoginPage> extends BasePageState<T> {
  int _topTextureId = -1;
  int _bottomLeftTextureId = -1;
  int _bottomRightTextureId = -1;
  int checkTextureId = -1;
  int uncheckTextureId = -1;

  @override
  void initState() {
    super.initState();
    loadBackgroundImageWithTexture();
  }

  void releaseTextures() {
    List<String> urls = [];
    urls.add('music_bg_part_top');
    urls.add('music_bg_part_bottom_left');
    urls.add('music_bg_part_bottom_right');
    urls.add('initial_check');
    urls.add('initial_uncheck');
    IvyBabyRecord.releaseList(urls);
  }

  @override
  bool onKeyCodeBackClick(BuildContext context) {
    releaseTextures();
    return super.onKeyCodeBackClick(context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget buildContent(BuildContext context) {
    return Stack(
      alignment: Alignment.center,

      children: [
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  Color(0xFF6EC67F),
                  Color(0xFF57CEA9),
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter
            ),
          ),
        ),

        Positioned(
          width: ScreenUtil().setWidth(186),
          height: ScreenUtil().setWidth(70),
          top: 0,
          left: ScreenUtil().setWidth(28),
          child: UIUtils.getWidgetByTextureId(_topTextureId),
        ),

        Positioned(
          width: ScreenUtil().setWidth(145),
          height: ScreenUtil().setWidth(80),
          bottom: 0,
          left: 0,
          child: UIUtils.getWidgetByTextureId(_bottomLeftTextureId),
        ),

        Positioned(
          width: ScreenUtil().setWidth(159),
          height: ScreenUtil().setWidth(139),
          bottom: 0,
          right: 0,
          child: UIUtils.getWidgetByTextureId(_bottomRightTextureId),
        ),

        buildDetailContent(context),
      ],
    );
  }

  Widget buildDetailContent(BuildContext context);

  Future<void> loadBackgroundImageWithTexture() async {
    int topId = -1;
    int bottomLeftId = -1;
    int bottomRightId = -1;
    int checkId = -1;
    int uncheckId = -1;
    try {
      topId = await IvyBabyRecord.loadImage('music_bg_part_top');
      bottomLeftId = await IvyBabyRecord.loadImage('music_bg_part_bottom_left');
      bottomRightId = await IvyBabyRecord.loadImage('music_bg_part_bottom_right');
      checkId = await IvyBabyRecord.loadImage('initial_check');
      uncheckId = await IvyBabyRecord.loadImage('initial_uncheck');
    } on PlatformException {
      print('loadImage exception!');
    }

    if (!mounted) return;

    setState(() {
      _topTextureId = topId;
      _bottomLeftTextureId = bottomLeftId;
      _bottomRightTextureId = bottomRightId;
      checkTextureId = checkId;
      uncheckTextureId = uncheckId;
    });
  }
}