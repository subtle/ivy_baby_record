import 'package:flutter/material.dart';
import 'package:ivy_baby_record/user/util/log.dart';

class RegLoginNotifier extends ChangeNotifier {
  String mobile = '';
  String code = '';
  String dialCode = '+86';
  bool isReg = false;
  bool isCountdown = false;
  bool agreement = false;
  bool isUsePswLogin = false;
  String countdown = '';
  int backId = -1;
  int wxId = -1;
  int qqId = -1;
  int weiBoId = -1;
  int aliId = -1;
  int hwId = -1;
  int downIndId = -1;

  void updateTextureIds(int bId, int wId, int qId, int wbId, int aId, int hId, int dId) {
    backId = bId;
    wxId = wId;
    qqId = qId;
    weiBoId = wbId;
    aliId = aId;
    hwId = hId;
    downIndId = dId;
    notifyListeners();
  }

  void updateMobile(String value) {
    mobile = value;
    notifyListeners();
  }

  void updateCode(String value) {
    code = value;
    notifyListeners();
  }

  void updateIsUsePswLogin(bool value) {
    isUsePswLogin = value;
    notifyListeners();
  }

  void updateIsAgree(bool value) {
    agreement = value;
    notifyListeners();
  }

  void updateIsReg(bool value) {
    isReg = value;
    notifyListeners();
  }

  void updateCountdown(String value) {
    countdown = value;
    notifyListeners();
  }

  void updateIsCountdown(bool value) {
    isCountdown = value;
    notifyListeners();
  }

  @override
  void dispose() {
    if (hasListeners) {
      Log.i('RegLoginNotifier', 'dispose');
      super.dispose();
    } else {
      Log.i('RegLoginNotifier', 'have disposed');
    }
  }
}