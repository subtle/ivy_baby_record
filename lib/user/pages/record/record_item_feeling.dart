import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ivy_baby_record/user/colors/colors.dart';
import 'package:ivy_baby_record/user/model/record/day_log.dart';
import 'package:ivy_baby_record/user/pages/record/record_item_image.dart';

class RecordItemFeeling extends StatefulWidget {
  final DayLog dayLog;

  RecordItemFeeling(this.dayLog);

  @override
  State<StatefulWidget> createState() => _RecordItemFeelingState();
}

class _RecordItemFeelingState extends State<RecordItemFeeling> {
  late DayLog _dayLog;

  @override
  void initState() {
    super.initState();
    _dayLog = widget.dayLog;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Offstage(
          offstage: (_dayLog.note ?? '').isEmpty,
          child: Text(
            _dayLog.note ?? '',
            style: TextStyle(color: CustomColors.c1, fontSize: 15),
          ),
        ),

        RecordItemImage(_dayLog),
      ],
    );
  }
}
