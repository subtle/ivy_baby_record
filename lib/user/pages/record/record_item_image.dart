import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ivy_baby_record/user/model/record/day_log.dart';

class RecordItemImage extends StatefulWidget {
  final DayLog dayLog;

  RecordItemImage(this.dayLog);

  @override
  State<StatefulWidget> createState() => _RecordItemImageState();
}

class _RecordItemImageState extends State<RecordItemImage> {
  late DayLog _dayLog;

  @override
  void initState() {
    super.initState();
    _dayLog = widget.dayLog;
  }

  @override
  Widget build(BuildContext context) {
    bool noImage = isNoImage();
    return Offstage(
      offstage: noImage,
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            noImage
                ? Container()
                : Image.network(_dayLog.imgs[0].url!, height: 68, width: 68),
            SizedBox(width: 5),
            Text(
              noImage ? '' : '共${_dayLog.imgs.length}张',
              style: TextStyle(color: Colors.white, fontSize: 15),
            )
          ],
        ),
      ),
    );
  }

  bool isNoImage() => _dayLog.imgs == null || _dayLog.imgs.length == 0;
}
