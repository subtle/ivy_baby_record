import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ivy_baby_record/user/model/record/day_log.dart';
import 'package:ivy_baby_record/user/pages/record/record_item_image.dart';
import 'package:ivy_baby_record/user/pages/record/record_util.dart';

class RecordItemMedicine extends StatefulWidget {
  final DayLog dayLog;

  RecordItemMedicine(this.dayLog);

  @override
  State<StatefulWidget> createState() => _RecordItemMedicineState();
}

class _RecordItemMedicineState extends State<RecordItemMedicine> {
  late DayLog _dayLog;

  @override
  void initState() {
    super.initState();
    _dayLog = widget.dayLog;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Offstage(
              offstage: (_dayLog.datainfo?.pills ?? '').isEmpty,
              child: Text(
                _dayLog.datainfo?.pills ?? '',
                style: TextStyle(color: Colors.white, fontSize: 15),
              ),
            ),
            Offstage(
              offstage: (_dayLog.datainfo?.dosage ?? '').isEmpty,
              child: Text(
                _dayLog.datainfo?.dosage ?? '',
                style: TextStyle(color: Colors.white, fontSize: 15),
              ),
            ),
          ],
        ),

        RecordUtil.getRecordNoteView(_dayLog),

        RecordItemImage(_dayLog),
      ],
    );
  }
}
