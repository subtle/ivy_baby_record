import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ivy_baby_record/user/colors/colors.dart';
import 'package:ivy_baby_record/user/model/record/day_log.dart';
import 'package:ivy_baby_record/user/pages/record/record_item_image.dart';
import 'package:ivy_baby_record/user/pages/record/record_util.dart';

class RecordItemWeight extends StatefulWidget {
  final DayLog dayLog;

  RecordItemWeight(this.dayLog);

  @override
  State<StatefulWidget> createState() => _RecordItemWeightState();
}

class _RecordItemWeightState extends State<RecordItemWeight> {
  late DayLog _dayLog;

  @override
  void initState() {
    super.initState();
    _dayLog = widget.dayLog;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: 72,
          child: Row(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        _dayLog.datainfo == null ? '未测量' : '${_dayLog.datainfo.weight}公斤',
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),

                      Text('体重', style: TextStyle(color: Colors.white, fontSize: 12))
                    ],
                  ),
                ),
              ),

              VerticalDivider(width: 1, color: CustomColors.record_vertical_divider),

              Expanded(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        _dayLog.datainfo == null ? '未测量' : '${_dayLog.datainfo.girth}厘米',
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),

                      Text('腹围', style: TextStyle(color: Colors.white, fontSize: 12))
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),

        Divider(height: 0.5, color: CustomColors.record_vertical_divider),

        SizedBox(height: 10),

        RecordUtil.getRecordNoteView(_dayLog),

        RecordItemImage(_dayLog),

        SizedBox(height: 10),
      ],
    );
  }
}
