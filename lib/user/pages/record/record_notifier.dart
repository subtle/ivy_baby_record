import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:ivy_baby_record/user/model/record/day_log.dart';
import 'package:ivy_baby_record/user/model/record/has_record.dart';
import 'package:ivy_baby_record/user/pages/record/record_util.dart';

class RecordNotifier extends ChangeNotifier {
  late String curDate;
  int calendarId = -1;
  late DateTime selectDate;
  HashMap<int, List<DayLog>> dataMap = HashMap<int, List<DayLog>>();
  LinkedHashMap<String, HasRecord> hasRecordMap = LinkedHashMap<String, HasRecord>();

  void updateCurDate(String value) {
    curDate = value;
    notifyListeners();
  }

  void updateSelectDate(DateTime value) {
    selectDate = value;
    notifyListeners();
  }

  void updateCalendarId(int value) {
    calendarId = value;
    notifyListeners();
  }

  void updateData(int index, List<DayLog> list) {
    dataMap[index] = list;
    notifyListeners();    
  }

  void addData(int index, List<DayLog> list) {
    if (dataMap[index] == null) {
      dataMap[index] = list;
    } else {
      dataMap[index]!.addAll(list);
    }
    RecordUtil.sortRecord(dataMap[index]!, true);
    notifyListeners();
  }
}