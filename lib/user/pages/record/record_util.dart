import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:ivy_baby_record/user/colors/colors.dart';
import 'package:ivy_baby_record/user/constants/constants.dart';
import 'package:ivy_baby_record/user/model/record/day_log.dart';
import 'package:ivy_baby_record/user/pages/record/record_item_feeling.dart';
import 'package:ivy_baby_record/user/pages/record/record_item_medicine.dart';
import 'package:ivy_baby_record/user/pages/record/record_item_weight.dart';
import 'package:ivy_baby_record/user/util/datetime.dart';

class RecordUtil {
  RecordUtil._();

  static Color getRecordItemColor(int type) {
    switch (type) {
      case Constants.TYPE_PREGNANCY_WEIGHT:
        return CustomColors.record_pregnant_weight;

      case Constants.TYPE_PREGNANCY_MEDICINE:
        return CustomColors.record_pregnant_medicine;

      case Constants.TYPE_PREGNANCY_FEELING:
        return CustomColors.record_feeling;

      default:
        return CustomColors.c1;
    }
  }

  static Widget getRecordNoteView(DayLog log) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: isNeedResetPadding(log) ? 10 : 0),
      child: Offstage(
        offstage: (log.note ?? '').isEmpty,
        child: Text(
          log.note ?? '',
          style: TextStyle(color: CustomColors.c1, fontSize: 13),
        ),
      ),
    );
  }

  static Widget getRecordItem(DayLog log) {
    if (log == null) {
      return Container();
    }

    switch (log.type) {
      case Constants.TYPE_PREGNANCY_WEIGHT:
        return RecordItemWeight(log);

      case Constants.TYPE_PREGNANCY_MEDICINE:
        return RecordItemMedicine(log);

      case Constants.TYPE_PREGNANCY_FEELING:
        return RecordItemFeeling(log);

      default:
        return Container();
    }
  }

  static bool isNeedResetPadding(DayLog log) {
    int type = log.type;
    return type == Constants.TYPE_PREGNANCY_WEIGHT ||
        type == Constants.TYPE_GROW || type == Constants.TYPE_PREGNANCY_BEAT;
  }

  static EdgeInsets getRecordItemPadding(DayLog log) {
    if (isNeedResetPadding(log)) {
      return EdgeInsets.all(0);
    }

    return EdgeInsets.all(10);
  }

  static void sortRecord(List<DayLog> list, bool asc) {
    if (list != null && list.length > 1) {
      list.sort((left, right) {
        if (asc) {
          return DateTimeUtil.getTimestamp(left.eventTime).compareTo(DateTimeUtil.getTimestamp(right.eventTime));
        }
        return DateTimeUtil.getTimestamp(right.eventTime).compareTo(DateTimeUtil.getTimestamp(left.eventTime));
      });
    }
  }

  static bool isSameType(DayLog l, DayLog r) {
    return (l.type == r.type) ||
        (l.type == Constants.TYPE_SLEEP && r.type == Constants.TYPE_WAKE) ||
        (l.type == Constants.TYPE_WAKE && r.type == Constants.TYPE_SLEEP);
  }

  static bool isSameId(DayLog l, DayLog r) {
    return (l.id > 0 && l.id == r.id) || (l.localId > 0 && l.localId == r.localId);
  }

  static bool isSameRecord(DayLog l, DayLog r) {
    if (l == null || r == null) {
      return false;
    }

    if (l != r) {
      return isSameId(l, r) && isSameType(l, r);
    }

    return true;
  }
}