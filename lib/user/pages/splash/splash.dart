import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:ivy_baby_record/user/api/login_util.dart';
import 'package:ivy_baby_record/user/pages/base/base_page.dart';
import 'package:ivy_baby_record/ivy_baby_record.dart';
import 'package:ivy_baby_record/user/util/ui_util.dart';
import 'package:ivybaby_api/device/app_device.dart';

class SplashPage extends BasePage {
  @override
  State<StatefulWidget> createState() => _SplashPageState();
}

class _SplashPageState extends BasePageState<SplashPage> {
  int _bgTextureId = -1;
  int _logoTextureId = -1;

  @override
  bool get showTitle => false;

  @override
  void initState() {
    super.initState();

    loadImageWithTexture();
    initPlatformState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, designSize: Size(375, 667));
    return super.build(context);
  }

  @override
  void dispose() {
    super.dispose();
    IvyBabyRecord.release('splash_bg');
    IvyBabyRecord.release('splash_logo');
  }

  @override
  Widget buildContent(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      fit: StackFit.loose,
      // overflow: Overflow.clip,
      children: [
        Positioned(
          left: 0,
          top: 0,
          right: 0,
          bottom: 0,
          child: Container(color: Colors.white)
        ),
        Container(
          alignment: Alignment.center,
          width: ScreenUtil().setWidth(340),
          height: ScreenUtil().setWidth(469),
          child: UIUtils.getWidgetByTextureId(_bgTextureId),
        ),
        Positioned(
          bottom: ScreenUtil().setWidth(28),
          width: ScreenUtil().setWidth(143),
          height: ScreenUtil().setWidth(43),
          child: UIUtils.getWidgetByTextureId(_logoTextureId)
        ),
      ],
    );
  }

  Future<void> loadImageWithTexture() async {
    int bgId = -1;
    int logoId = -1;
    try {
      bgId = await IvyBabyRecord.loadImage('splash_bg');
      logoId = await IvyBabyRecord.loadImage('splash_logo');
    } on PlatformException {
      print('loadImage exception!');
    }

    if (!mounted) return;

    setState(() {
      _bgTextureId = bgId;
      _logoTextureId = logoId;
    });
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    String clientVersion = '1.0.0';
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await IvyBabyRecord.platformVersion;
      clientVersion = await IvyBabyRecord.clientVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    AppDevice.inst.osVer = platformVersion;
    AppDevice.inst.clientVer = clientVersion;
    print('checkIsNeedInitChild clientVer : $clientVersion');
    LoginUtil.checkIsNeedInitChild(context);
  }
}