import 'package:ivy_baby_record/user/global/global_info.dart';
import 'package:ivy_baby_record/user/model/child.dart';

class ChildUtil {
  static bool isCurPregnant() {
    return isPregnant(GlobalInfo.inst.child);
  }

  static bool isPregnant(Child? child) {
    return child?.gestationStatus == 1;
  }
}