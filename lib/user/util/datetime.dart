import 'package:intl/intl.dart';

class DateTimeUtil {
  DateTimeUtil._();

  static String formatYMDChinese(DateTime dateTime) {
    return DateFormat('yyyy年MM月dd日').format(dateTime);
  }

  static String formatDefaultDate(DateTime dateTime) {
    return DateFormat('yyyy-MM-dd HH:mm:ss').format(dateTime);
  }

  static String formatYMDDate(DateTime dateTime) {
    return DateFormat('yyyy-MM-dd').format(dateTime);
  }

  static String formatDate(String format, DateTime dateTime) {
    return DateFormat(format).format(dateTime);
  }

  static DateTime getStartDateTime(DateTime dateTime) {
    if (dateTime == null) {
      dateTime = DateTime.now();
    }
    DateTime start = DateTime(dateTime.year, dateTime.month, dateTime.day);
    return start;
  }

  static bool isSameDay(DateTime l, DateTime r) {
    return getStartDateTime(l).difference(getStartDateTime(r)).inDays == 0;
  }
  
  static int getTimestamp(String dateTimeStr) {
    return DateTime.parse(dateTimeStr).millisecondsSinceEpoch;
  }
}