import 'package:flutter/cupertino.dart';

class Log {
  Log._();

  static void i(String tag, String msg) {
    debugPrint('$tag : $msg');
  }
}