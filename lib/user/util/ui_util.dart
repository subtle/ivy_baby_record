import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:ivy_baby_record/user/colors/colors.dart';

import '../../ivy_baby_record.dart';

typedef DateUpdateCallback = void Function(DateTime dateTime);

class UIUtils {
  static Widget getWidgetByTextureId(int id) {
    return id < 0 ? Text('') : Texture(textureId: id);
  }

  static Future<Map<String, int>> loadImageTexture(List<String> list) async {
    Map<String, int> resultMap = {};
    try {
      for (String image in list) {
        int textureId = await IvyBabyRecord.loadImage(image);
        if (textureId >= 0) {
          resultMap[image] = textureId;
        }
      }
    } on PlatformException {
      print('loadImage exception!');
    }
    return Future<Map<String, int>>(() => resultMap);
  }

  static void showCustomDatePicker(BuildContext context, bool gestation,
      DateUpdateCallback callback) {
    // DateTime dateTime = DateTime.now();
    // DateTime minDateTime = gestation
    //     ? dateTime
    //     : DateTime(dateTime.year - 14, dateTime.month, dateTime.day);
    //
    // DatePicker.showDatePicker(context,
    //   pickerTheme: DateTimePickerTheme(
    //     showTitle: true,
    //     confirm: Text('确定', style: TextStyle(color: CustomColors.c8)),
    //     cancel: Text('取消', style: TextStyle(color: Colors.black38)),
    //   ),
    //   minDateTime: minDateTime,
    //   maxDateTime: gestation ? dateTime.add(new Duration(days: 280)) : dateTime,
    //   initialDateTime: dateTime,
    //   dateFormat: 'yyyy年-MM月-dd日',
    //   locale: DateTimePickerLocale.zh_cn,
    //   onCancel: () => print('onCancel'),
    //   onClose: () => print('onClose'),
    //   onChange: (dateTime, index) {
    //     print('onChange dateTime : $dateTime');
    //   },
    //   onConfirm: (dateTime, index) {
    //     print('onConfirm dateTime : $dateTime');
    //     callback?.call(dateTime);
    //   }
    // );
  }
}