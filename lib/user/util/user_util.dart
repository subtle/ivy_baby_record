import 'package:ivy_baby_record/user/constants/constants.dart';
import 'package:ivy_baby_record/user/global/global_info.dart';
import 'package:ivybaby_api/api/config/global_data.dart';

class UserUtils {
  UserUtils._();

  static bool isGuest() {
    return (GlobalInfo.inst.childId ?? '').isEmpty ||
        Constants.TYPE_GUEST.toString() == GlobalData.appDevice.loginType;
  }

  static String childId() => GlobalInfo.inst.childId;
}
