
import 'package:ivy_baby_record/user/model/child.dart';

class Util {
  static int parseInt(String value) {
    try {
      return int.parse(value);
    } on Exception catch (e) {
      print('Unknown exception: $e');
      return 0;
    } catch (e) {
      print('Something really unknown:: $e');
      return 0;
    }
  }

  static void sortChild(List<Child>? list, bool asc) {
    if (list != null && list.length > 1) {
      list.sort((left, right) {
        if (asc) {
          return left.birthday.compareTo(right.birthday);
        }
        return right.birthday.compareTo(left.birthday);
      });
    }
  }
}