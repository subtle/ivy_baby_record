import 'package:flutter/material.dart';

class FlatButton extends StatefulWidget {
  const FlatButton({
    super.key,
    this.textColor,
    this.fontSize,
    this.backgroundColor,
    this.disabledTextColor,
    this.fontWeight,
    this.shape,
    this.minimumSize,
    this.padding,
    this.selected,
    required this.onPressed,
    required this.child,
  });

  /// 按钮背景颜色
  final Color? backgroundColor;
  /// shape
  final OutlinedBorder? shape;
  /// size
  final Size? minimumSize;
  /// padding
  final EdgeInsetsGeometry? padding;

  final Color? textColor;

  final Color? disabledTextColor;

  final double? fontSize;

  final FontWeight? fontWeight;

  final bool? selected;

  final VoidCallback? onPressed;

  final Widget child;

  @override
  State<StatefulWidget> createState() {
    return _FlatButtonState();
  }
}

class _FlatButtonState extends State<FlatButton> {
  late final MaterialStatesController statesController;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      // statesController: statesController,
      style: TextButton.styleFrom(
        primary: widget.textColor,
        shape: widget.shape,
        foregroundColor: widget.textColor,
        backgroundColor: widget.backgroundColor,
        disabledForegroundColor: widget.disabledTextColor,
        minimumSize: widget.minimumSize,
        padding: widget.padding,
        textStyle: TextStyle(
          fontSize: widget.fontSize,
          fontWeight: widget.fontWeight,
        )
      ),
      onPressed: widget.onPressed,
      child: widget.child,
    );
  }

  @override
  void initState() {
    super.initState();
    // statesController = MaterialStatesController(
    //     <MaterialState>{if (widget.selected??false) MaterialState.selected});
  }

  @override
  void didUpdateWidget(FlatButton oldWidget) {
    super.didUpdateWidget(oldWidget);
    // if (widget.selected != oldWidget.selected) {
    //   statesController.update(MaterialState.selected, widget.selected??false);
    // }
  }
}